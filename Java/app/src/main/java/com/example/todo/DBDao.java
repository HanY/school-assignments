package com.example.todo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

//########### The database dao ########################
// Function summary
// Add a column -> addTask(Task task)
// Delete a column -> int updateTask(Task task)
// Update a column -> int updateTask(Task task)
// Return all tasks -> List<Task> getAllTasks()
//
/*
|||||||||||||||||||||||||||||||||||
||    ID   |Task Name |  Status  ||
|||||||||||||||||||||||||||||||||||
||    0    |    Yes   |    0     ||
||         |          |          ||
||         |          |          ||
||         |          |          ||
|||||||||||||||||||||||||||||||||||
 */
public class DBDao extends SQLiteOpenHelper{
    // ### Start of static sql query language strings #####################
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "FeedReader.db";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + FeedEntry.TABLE_NAME + " (" +
                    FeedEntry._ID + " INTEGER PRIMARY KEY," +
                    FeedEntry.COLUMN_NAME_TITLE + " TEXT," +
                    FeedEntry.COLUMN_NAME_SUBTITLE + " INTEGER)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + FeedEntry.TABLE_NAME;


    public static class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "Task";
        public static final String COLUMN_NAME_TITLE = "Name";
        public static final String COLUMN_NAME_SUBTITLE = "Status";
    }
    // ### End of it ######################
    public DBDao(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        Log.d("DB","Create");
        db.execSQL(SQL_CREATE_ENTRIES);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void addTask(Task task) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(FeedEntry.COLUMN_NAME_TITLE, task.getTaskName());
        values.put(FeedEntry.COLUMN_NAME_SUBTITLE, task.getStatus());
// Inserting Row
        db.insert(FeedEntry.TABLE_NAME, null, values);
        db.close(); // Closing database connection
    }

    public List<Task> getAllTasks() {
        List<Task> TaskList = new ArrayList<Task>();
// Select All Query
        String selectQuery = "SELECT * FROM " + FeedEntry.TABLE_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
// looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Task task = new Task();
                task.setId(Integer.parseInt(cursor.getString(0)));
                task.setTaskName(cursor.getString(1));
                task.setStatus(cursor.getInt(2));
// Adding contact to list
                TaskList.add(task);
            } while (cursor.moveToNext());
        }
// return contact list
        return TaskList;
    }

    public int updateTask(Task task) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(FeedEntry.COLUMN_NAME_TITLE, task.getTaskName());
        values.put(FeedEntry.COLUMN_NAME_SUBTITLE, task.getStatus());
// updating row
        return db.update(FeedEntry.TABLE_NAME, values, FeedEntry._ID+ " = ?",
                new String[]{String.valueOf(task.getId())});
    }

    public void deleteTask(Task task) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(FeedEntry.TABLE_NAME, FeedEntry._ID + " = ?",
                new String[] { String.valueOf(task.getId()) });
        db.close();
    }
}
