package com.example.todo;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import org.apache.commons.io.FileUtils;

import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    private ArrayList<String> items;    // List to supply adapter view
    private ListView listView;
    private List<Task> task;            // The real task array
    private ArrayAdapter<String> adapter;
    private DBDao DBDao;                // SQL DB DAO
    private TextView status;            // Currently useless

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //################# Init Everything ###################

        DBDao = new DBDao(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        status = findViewById(R.id.status);
        listView = findViewById(R.id.lvItems);
        items = new ArrayList<String>();
        task = DBDao.getAllTasks();
        for (Task mTask : task){
            items.add(mTask.getTaskName());
        }
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);
        listView.setAdapter(adapter);
        // ######### Edit task name handler (single click)
        editslot();
        // ######### Delete task handler (long click)
        deleteslot();
    }

    public void onAddItem(View v) {
        EditText etNewItem = (EditText) findViewById(R.id.etNewItem);
        String itemText = etNewItem.getText().toString();
        // ### create new task instance and add it into database
        DBDao.addTask(new Task(itemText,0));
        // ### update adapter directly from user input
        adapter.add(itemText);
        adapter.notifyDataSetChanged();
        // ### clear text
        etNewItem.setText("");

    }

    // ######### Edit task name handler (single click)

    private void editslot() {
        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> m_adapter,
                                            View item, int pos, long id) {
                        get_txt_pop(pos); // Pop up a window and get new text name
                    }
                });
    }

    // ######### Delete task handler (long click)

    private void deleteslot() {
        listView.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> m_adapter,
                                                   View item, int pos, long id) {

                        // ### get the soon to be deleted task's position in the array
                        DBDao.deleteTask(task.get(pos));
                        task.remove(pos); // ### remove task itself from array
                        items.remove(pos); // ### remove task name in adapter's array

                        adapter.notifyDataSetChanged();

                        return true;
                    }
                });
    }


    private void get_txt_pop(final int pos) {
        //############# start of pop up window construction ################
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("New Task Name");
        final EditText input = new EditText(this);
        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            // ########## Change task name ####################
            public void onClick(DialogInterface dialog, int which) {
                items.set(pos, input.getText().toString()); // ### set adapter's task name
                adapter.notifyDataSetChanged();
                task.get(pos).setTaskName(input.getText().toString()); // ### task array name
                DBDao.updateTask(task.get(pos)); // ### change to database
            }
        });
        // ### if canceled do nothing
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        // ### Display the pop up window
        builder.show();
    }
}
