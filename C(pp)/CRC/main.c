#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <ctype.h>
static unsigned int crc = 0;
static unsigned int polynomial = 0x1021;
static unsigned long fileLen;
unsigned int doCrc(unsigned char *data, int size)
{
  int i, j;
  for (i = 0; i < size; i++) {
    unsigned int xr = data[i] << 8;
    crc = crc ^ xr;

    for (j = 0; j < 8; j++)
    {
      if (crc & 0x8000) {
        crc = (crc << 1);
        crc = crc ^ polynomial;
      }
      else {
        crc = crc << 1;
      }
    }
  }
  crc = crc & 0xFFFF;
  return crc;
}
char* load_file(char* path){
    int fd;
    char* buffer;
    fd = open(path, O_RDONLY); // fuck windows
    if(fd == -1)
        abort();
    FILE* f = fdopen(fd, "r" ); // how about that?
    if (!f) {
        printf("can't open file\n");
        abort();
    }
    //Get file length
	fseek(f, 0, SEEK_END);
	fileLen = ftell(f);
	fseek(f, 0, SEEK_SET);
	//Allocate memory
	buffer=(unsigned char*)malloc(fileLen+1);
	if (!buffer)
	{
		fprintf(stderr, "Memory error!\n");
        fclose(f);
		abort();
	}
    fread(buffer, fileLen, 1, f);
    fclose(f);
    return buffer;
}

void helper(){
    printf("Usage: crc [OPTIONS] ARG\n");
    printf("OPTIONS:\n");
    printf("    -f    <FILENAME>            use file as input\n");
    printf("    -s    <STRING>              use string as input\n");
    printf("    -p    <POLYNOMIAL>          polynomial in hex, eg. 0x1021\n");
    printf("                                default: 0x1021\n");
    printf("    -i    <INIT>                CRC init in hex, eg. 0x1d0f\n");
    printf("                                default: 0x0000\n");
    abort();
}

int
main(int argc, char *argv[])
{
    int n;
    unsigned char *buffer = NULL;
    char* file_name = NULL;
    for(n=1; n < argc; n++){ // parse libraries GNU offered are just shit for this use case.
        if(strcmp("-f", argv[n]) == 0){
            file_name = argv[n+1];
            buffer = load_file(argv[n+1]);
        }
        else if(strcmp("-s", argv[n]) == 0){
            buffer = argv[n+1];
            fileLen = strlen(buffer);
            file_name = argv[n+1];
        }
        else if(strcmp("-p",argv[n]) == 0){
            polynomial = (unsigned int)strtol(argv[n+1], NULL, 16);
        }
        else if(strcmp("-i",argv[n]) == 0){
            crc = (unsigned int)strtol(argv[n+1], NULL, 16);
        }
    }
    if (file_name == NULL || buffer == NULL)
        helper();
	printf("The crc Checksum of %s is 0x%04X\n", file_name, doCrc(buffer, fileLen));
    printf("goodbye\n");
}
