#include <errno.h>
#include <ncurses.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#define MAXCLIENTS 100
#define BUFFER 512000
static int client_socketfd[MAXCLIENTS] = {0};
static char recv_buf[BUFFER];
static char *nickname = NULL;
static char announcement[] = "entered the chat\n";
static char *serverIP = NULL;
int remote_port = 6969;

struct two_windows {
  WINDOW *win1;
  WINDOW *win2;
};

static void reply_all(int fd, char *buf, int len);
void helper();
void *control_thread(void *window);
int main(int argc, char *argv[]) {
  int listen_sockfd, new_sockfd, n, i, fd, row, col, client_only = FALSE,
                                                     portno = 6969, max_fd = 0;
  socklen_t clilen;
  struct sockaddr_in serv_addr, cli_addr;
  fd_set readfds;
  pthread_t input;
  WINDOW *outputWin, *inputWin;
  struct two_windows tWin;

  if (argc == 1) {
    helper();
    return 0;
  }

  for (n = 1; n < argc;
       n++) { // parse libraries GNU offered are just shit for this use case.
    if (strcmp("-l", argv[n]) == 0 || strcmp("--listen-port", argv[n]) == 0) {
      portno = atoi(argv[n + 1]);
    } else if (strcmp("-s", argv[n]) == 0 ||
               strcmp("--remote-server", argv[n]) == 0) {
      serverIP = argv[n + 1];
    } else if (strcmp("-p", argv[n]) == 0 ||
               strcmp("--remote-port", argv[n]) == 0) {
      remote_port = atoi(argv[n + 1]);
    } else if (strcmp("-c", argv[n]) == 0 ||
               strcmp("--client-only", argv[n]) == 0) {
      client_only = TRUE;
    } else if (strcmp("-n", argv[n]) == 0 ||
               strcmp("--nickname", argv[n]) == 0) {
      if (strlen(argv[n + 1]) > 127)
        argv[n + 1][127] = '\0';
      nickname = argv[n + 1];
    } else if ((strcmp("-h", argv[n]) == 0 || strcmp("--help", argv[n]) == 0)) {
      helper();
      return 0;
    }
  }

  if (nickname == NULL) {
    printf("You must specify a nickname\n");
    return 0;
  }
  if (serverIP != NULL) {
    struct hostent *remote;
    char buf[196];
    memset(buf, 0, sizeof(buf));
    int remotefd = socket(AF_INET, SOCK_STREAM, 0);
    if (remotefd < 0) {
      perror("ERROR opening socket");
      exit(1);
    }

    remote = gethostbyname(serverIP);
    if (remote == NULL) {
      fprintf(stderr, "ERROR, no such host\n");
      exit(1);
    }

    memset((void *)&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)remote->h_addr, (char *)&serv_addr.sin_addr.s_addr,
          remote->h_length);
    serv_addr.sin_port = htons(remote_port);
    if (connect(remotefd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) <
        0) {
      perror("ERROR connecting");
      exit(1);
    }
    client_socketfd[0] = remotefd;
    strcat(buf, "Server: ");
    strcat(buf, nickname);
    strcat(buf, " ");
    strcat(buf, announcement);
    write(remotefd, buf, strlen(buf) + 1);
  }
  if (!client_only) {
    listen_sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (listen_sockfd < 0) {
      perror("ERROR opening socket");
      exit(1);
    }
    memset((void *)&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);
    if (bind(listen_sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) <
        0) {
      perror("ERROR on binding");
      exit(1);
    }
    listen(listen_sockfd, 5);
  }
  clilen = sizeof(serv_addr);

  initscr();
  getmaxyx(stdscr, row, col);
  outputWin = newwin(row - 1, col, 0, 0);
  inputWin = newwin(1, col, row - 1, 0);
  tWin.win1 = outputWin;
  tWin.win2 = inputWin;
  scrollok(outputWin, TRUE);
  wprintw(outputWin, "Server: type quit() to exit\n");
  wprintw(outputWin, "Server: DO NOT RESIZE YOUR WINDOW!!!!!!\n");
  wrefresh(outputWin);
  pthread_create(&input, NULL, control_thread, (void *)&tWin);
  while (TRUE) {
    memset(recv_buf, 0, BUFFER);
    FD_ZERO(&readfds);
    if (!client_only) {
      FD_SET(listen_sockfd, &readfds);
      max_fd = listen_sockfd;
    } else
      max_fd = 0;
    for (i = 0; i < MAXCLIENTS; i++) // determine max socketfd
    {
      fd = client_socketfd[i];
      if (fd > 0) // append valid socket to listen struct
        FD_SET(fd, &readfds);
      else
        continue;
      if (fd > max_fd)
        max_fd = fd;
    }
    n = select(max_fd + 1, &readfds, NULL, NULL, NULL);

    if ((n < 0) && (errno != EINTR)) {
      wprintw(outputWin, "Server: select error\n");
      wrefresh(outputWin);
    }
    if (!client_only) {
      if (FD_ISSET(listen_sockfd, &readfds)) {
        if ((new_sockfd = accept(listen_sockfd, (struct sockaddr *)&serv_addr,
                                 &clilen)) < 0) {
          wprintw(outputWin, "Server: accept error\n");
          wrefresh(outputWin);
        }
      }

      // add new socket to array of sockets
      for (i = 0; i < MAXCLIENTS; i++) {
        // if position is empty
        if (client_socketfd[i] == 0) {
          client_socketfd[i] = new_sockfd;
          break;
        }
      }
    }
    for (i = 0; i < MAXCLIENTS; i++) {
      fd = client_socketfd[i];

      if (FD_ISSET(fd, &readfds)) {
        // Check if it was for closing , and also read the incoming message
        if ((read(fd, recv_buf, BUFFER)) <= 0) {

          // Close the socket and mark as 0 in list for reuse
          close(fd);
          client_socketfd[i] = 0;
        }

        else {
          wprintw(outputWin, "%s\n", recv_buf);
          wrefresh(outputWin);
          reply_all(fd, recv_buf, strlen(recv_buf));
        }
      }
    }
  }
}

void reply_all(int fd, char *buf, int len) {
  int j, n = 0;
  socklen_t clilen;
  struct sockaddr_in cli_addr;
  for (n; n < MAXCLIENTS; n++) {
    if (client_socketfd[n] != fd && client_socketfd[n] > 0) {
      j = write(client_socketfd[n], buf, len + 1);
      if (j <= 0) {
        close(client_socketfd[n]);
        client_socketfd[n] = 0;
      }
    }
  }
}
void helper() {
  printf("Usage: chatty [OPTIONS] ARG\n");
  printf("OPTIONS:\n");
  printf("    -l, --listen-port    <PORT NUMBER>            which port to\n");
  printf("                                                  listen\n");
  printf(
      "    -p, --remote-port    <PORT NUMBER>            remote server port\n");
  printf(
      "    -n, --nickname       <NAME>                   your nickname, max\n");
  printf("                                                  length 127\n");
  printf("    -s, --remote-server  <ADDRESS>                remote server "
         "address\n");
  printf("    -c, --client-only                            "
         " client only mode\n");
  printf("    -h, --help                                    print this help\n");
}
void *control_thread(void *window) {
  char buf[BUFFER - 127], reply[BUFFER];
  struct two_windows *mWindows = (struct two_windows *)window;
  WINDOW *outputWin = mWindows->win1;
  WINDOW *inputWin = mWindows->win2;
  wprintw(inputWin, "%s: ", nickname);
  wrefresh(inputWin);
  while (TRUE) {
    memset(buf, 0, sizeof(buf));
    memset(reply, 0, sizeof(reply));
    wgetnstr(inputWin, buf, BUFFER);
    if (!strcmp("quit()", buf)) {
      endwin();
      pthread_exit(NULL);
    }
    wmove(inputWin, 0, 0);
    wclrtoeol(inputWin);
    wprintw(inputWin, "%s: ", nickname);
    wrefresh(inputWin);
    strcat(reply, nickname);
    strcat(reply, ": ");
    strcat(reply, buf);
    wprintw(outputWin, "%s\n", reply);
    wrefresh(outputWin);
    reply_all(0, reply, strlen(reply));
  }
}
